# Guía para obtener Token de Autenticación

**1.** Para obtener `AuthToken` dirigete a: https://auth.bitsenda.com/signin y autentícate con:

- **Email:** `soporte+_testing@coinsenda.com`
- **Password:** `1231233`

**2.** Abre la consola de desarrollo y obtén el token, así de simple...

*Recuerda que el token de autenticación tiene un tiempo de expiración apróximado de 5 mínutos.*
