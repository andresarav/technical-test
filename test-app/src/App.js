import { useEffect } from 'react'
import {
  AUTH_TOKEN,
  USER_ID,
  DEPOSIT_URL
 } from './const'

function App() {

  const handleError = err => {
    // TODO: Create a method to handle error
    console.log(err)
    return
  }

  const doFetch = async(url) => {
    try {
      const response = await fetch(`${DEPOSIT_URL}users/${USER_ID}/deposits`, {
        method: `GET`,
        headers:{
          Authorization: `Bearer ${AUTH_TOKEN}`,
        }
      });
      const finalResponse = await response.json();
      console.log(await finalResponse)
      return await finalResponse;
    } catch (_) {
      return handleError(_)
    }
  }

  useEffect(() => {
    doFetch()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])


  return (
    <>
    App testing
    </>
  );
}

export default App;
